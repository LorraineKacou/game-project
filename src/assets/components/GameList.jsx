import React from "react";
import axios from "axios";
import { useState, useEffect } from "react";
import GameItem from "./GameItem";


function GameList() {

    const [games, setGames] = useState([]);

    useEffect(function () {
        const API_URL = "https://raw.githubusercontent.com/bastienapp/gamesapi/main/db.json";
        axios
            .get(API_URL)
            .then((response) => setGames(response.data))
            .catch((error) => console.error(error));
    }, []);


    return (
        <div>

            {games.map((game, index) => {
                return (
                    <li key={index}>
                        <GameItem
                            name={game.name}
                            rating={game.rating}
                            released={game.released}
                            background_image={game.background_image}
                            clip={game.clip.clip}

                        />
                    </li>
                );
            })}
        </div>
    );
}

export default GameList;
