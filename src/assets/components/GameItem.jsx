import React from "react";

function GameItem(props) {
    const { name, rating, released, background_image, clip } = props;

    return (
        <div className="GameItem">
            <h3>{name}</h3>
            <h3>{rating}</h3>
            <h3>{released}</h3>
            <h3>{background_image}</h3>
            <h3>{clip}</h3>
            <div>_________________________________</div>
        </div>
    );
}

export default GameItem;