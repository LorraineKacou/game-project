import React from "react";

function Footer(props) {
    return (
        <div className="Footer">
            <input type="search" text="search" className="search__input" />
            <button>{props.content}</button>
        </div>
    )
}

export default Footer;
