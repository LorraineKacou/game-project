import React from "react";
function Accueil(props) {
    return (
        <div className="Accueil">

            <h1>{props.content}</h1>
            <h2>{props.titre}</h2>
            <h2>{props.descriptif}</h2>
            <h2>{props.lien}</h2>
        </div>
    )
}

export default Accueil;