
import './App.css'
import Footer from './assets/components/Footer'
import GameList from './assets/components/GameList'
import Header from './assets/components/Header'
import Accueil from './assets/Pages/Accueil'
import JeuSpecifique from './assets/Pages/JeuSpecifique'
import ListeJeux from './assets/Pages/ListeJeux'

function App() {

  const movie = {
    content: "JeuSpecifique",
    titre: "Grand Theft Auto V",
    date: "2013-09-17",
    genre: "Action",
    poster:
      "https://media.rawg.io/media/games/b8c/b8c243eaa0fbac8115e0cdccac3f91dc.jpg",
  };


  return (
    <div className="App">
      <Header content="HUNT FOR GAMES" />
      <Accueil content="Accueil" titre="hunt for games" descriptif="Avec Hunt for games trouvez la perle rare au milieu d'un océan de jeux vidéo " lien="Afficher la liste" />

      <ListeJeux content="ListeJeux" titre="portal-2" title="Shooter" name="Puzzle" img="" />

      <JeuSpecifique content="JeuSpecifique" titre="Grand Theft Auto V" date="2013-09-17" img="" genre="Action" video="" />

      <JeuSpecifique {...movie} />
      <GameList />
      <Footer content="search" />

    </div>
  )
}

export default App
