import React from "react";
import { useState } from "react";

function JeuSpecifique(props) {

    const { content, titre, date, genre, poster } = props;
    const [favorite, setfavorite] = useState(false);


    return (
        <div className="JeuSpecifique">

            <h1>{props.content}</h1>
            <h2>{props.titre}</h2>
            <h2>{props.date}</h2>
            <img className="grand-theft-auto-v" src="https://media.rawg.io/media/games/b8c/b8c243eaa0fbac8115e0cdccac3f91dc.jpg" alt="grand-theft-auto-v" />
            <h2>{props.genre}</h2>
            <h2>{props.galerie_image}</h2>
            <video controls width="500">
                <source src="https://media.rawg.io/media/stories-640/5b0/5b0cfff8c606c5e4db4f74f108c4413b.mp4" />
            </video>


            <h2>content: {content}</h2>
            <h2>titre: {titre}</h2>
            <h2>date:{date}</h2>
            <h2>genre:{genre}</h2>

            <div>
                <img className="poster" src={poster} alt={titre} />
            </div>

            <button onClick={() => {
                if (favorite) {
                    setfavorite(false);
                } else { setfavorite(true); }
            }}
            >
                {favorite ? "Retirer de mes favoris" : "Ajouter à mes favoris"}
            </button>

        </div>
    )
}

export default JeuSpecifique;