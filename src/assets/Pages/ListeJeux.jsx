import React from "react";
function ListeJeux(props) {
    return (
        <div className="ListeJeux">

            <h1>{props.content}</h1>
            <h2>{props.titre}</h2>
            <img className='portal' src={"https://media.rawg.io/media/games/328/3283617cb7d75d67257fc58339188742.jpg"} alt='portal-2' />
            <h2>{props.title}</h2>
            <img className='Shooter' src={"https://media.rawg.io/media/games/198/1988a337305e008b41d7f536ce9b73f6.jpg"} alt='Shooter' />
            <h2>{props.name}</h2>
            <img className='Puzzle' src={"https://media.rawg.io/media/screenshots/aa7/aa7cc3831b020d2830a6e4cf7b40a73b.jpeg"} alt='Puzzle' />
        </div>
    )
}

export default ListeJeux;